# Recetas

## Recetas escritas

[* Pretzels con azucar y canela](/pretzelsAzucarCanela.md)

[* Postre de manzana](/postreManzana.md)

[* Bastoncitos de papa y queso](/bastoncitosDePapaYQueso.md)

## Recetas pendientes

* Churros

* Pretzels de queso

* Scones de queso

* Panqueques

* Bombitas

* Lemon pie

* Rolls de canela

* Cheese cake

* Pizza
