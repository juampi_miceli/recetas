# Ingredientes:

## Base
* 1 kilo de manzanas verdes
* 100gm de azúcar

## Licuado
* 2 tazas de leche si sos un niño como yo. O de vino blanco si sos un señor francés.
* 3 huevos
* 1 taza de azúcar
* 1 cucharadita de extracto de vainilla
* 50gm de harina
* 50gm de manteca

## Topping
* Canela a gusto
* Helado o crema chantilly

# Pasos a seguir:

## Idealmente vas a necesitar un horno a 140 grados. Andá precalentandolo.

## Base

1) Pelate el kilo de manzanas y cortalas en rodajas finas.

1) Poné las rodajas en una fuente alta y ancha (o en tu bello arno multichef) cubriendo toda la superficie lo más uniforme que puedas. Cada vez que terminas una capa lo espolvoreás con azúcar.

## Licuado

1) Cocina esa belleza por 10 minutos (Si tenés el maravilloso arno de multichef hacelo con la rejilla cerrada).

1) Licua todos los ingredientes y tirá ese licuado sobre las manzanas cocidas.

1) Cocina otros 10 minutos (rejilla abierta pa' los chetos).

## Topping

1) Espolvorea con la cantidad de canela que quieras el postre.

1) Servite esto calentito con helado de crema americana o con crema chantilly y sos dios.
