# Ingredientes:

## Masa

* 250 gramos de agua tibia (50 grados aprox)
* 1 paquete de levadura en polvo (7 gramos)
* 15 gramos de azúcar común
* 55 gramos de azúcar morena
* 2 cucharas de aceite
* 1 cucharadita de sal
* 500 gramos de harina leudante

## Baño en agua

* 1 taza de agua casi hirviendo
* 2 cucharadas de bicarbonato de sodio

## Topping

* 200gramos de azúcar común
* 2 cucharadas de canela
* 100 gramos de manteca pomada
* 1 cucharadita de extracto de vainilla

# Pasos a seguir:

## Poné a precalentar el horno en 230 grados c:


## Levar el agua :)

1) Agregá el agua tibia, el azúcar común y la levadura en un recipiente chico y alto. Por ejemplo un medidor.
1) Dejá reposar en un lugar cálido por 30 minutos (arriba del horno es un golazo).


## Hacer la masa:

1) Agregá la mezcla que hiciste en un bowl grande y mezclalo con el azúcar morena, el aceite y la sal hasta que quede uniforme.

1) Empezá a agregar y revolver la harina hasta que se te forme una masa copada (la masa debería quedar un poquito pegajosa, pero no mucho).

1) Tirale un poquito de aceite a un bowl y poné la masa en el bowl. Asegurate que toda la masa quede con un poquito de aceite para que no se pegue al bowl.

1) Cubrí el bowl con un repasador y dejalo levando en un lugar cálido por 45 minutos (arriba del horno es otro golazo).


## Hacer los bollos:

1) Enmantecá 2 fuentes para horno.

1) De la forma que más te guste empeza a hacer los bollos. Lo ideal es que queden entre 40 y 50 y que sean todos del mismo tamaño.

1) Mezclá el agua casi hirviendo con el bicarbonato de sodio.

1) Andá pasando los bollos armados, por el agua con bicarbonato y dejalos 10 segundos cada uno. Luego sacales el exceso de agua y andá poniendolos en las fuentes para horno. **IMPORTANTE:** Si te pasás de tiempo te va a quedar asqueroso. Si lo pones menos tiempo va a ser un pancito, no un pretzel.

1) Mandá los pretzels al horno por 8 minutos o hasta que estén dorados.


## Topping:

1) Mientras los pretzels están en el horno derretí la manteca en el microondas a potencia baja.

1) Mezclá el azúcar común con la canela y ponelas en un platito o un tupper grande y bajito.

1) Mezclá la manteca derretida con la cucharadita de extracto de vainilla.

1) Bollo por bollo pasalos por la manteca primero y por la mezcla de azúcar y canela después.

1) Comete el primer bollo que hagas para poder ver las estrellas y los demás devolvelos a la misma fuente para llevarselo a otra persona y que también vea las estrellas.
