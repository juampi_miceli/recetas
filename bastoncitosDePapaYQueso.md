# Ingredientes:

* 1 kilo de papa
* 250gm de maizena
* 300gm del queso rallado que más te guste
* Sal, pimienta, ajo en polvo y pimenton para condimentar

# Cosas extras que vas a necesitar:

* Papel manteca 
* Pisa papas
* Lugar en el freezer para una fuente de lasaña
* Esta receta está muy buena para tener un par de dips a mano. (Ej: Mayonesa, Alioli, Barbacoa, Cheddar, Ciboulette)

# Pasos a seguir:

## Masa

1) Pelate el kilo de papas, cortalas en pedazitos y mandalas a hervir, ponele sal gruesa al agua para que flasheen spa.

2) Cuando ya están archie mega suavecitas sacalas y pisalas bien con el pisa papas para hacer puré.

3) Ahora tirá todo lo que tengas ahí, los condimentos a gusto (sal, pimienta, ajo en polvo y pimentón), los 250gm de maizena y los 300gm de queso.

4) Mezcla, mezcla y seguí mezclando hasta conseguir una hermosa masa uniforme.

5) Agarrate una fuente tipo de lasaña y metele papel manteca en la base (no lo cortes), ahora tirale la masa sobre el papel manteca, aplastá hasta que te quede uniforme y cerrá el papel manteca de forma de tapar toda la masa por arriba.

6) IMPORTANTE: Agarrate el pisa papas y presioná super bien sobre el papel manteca, para que quede bien compacto y sin aire. Esto es muy importante porque de no hacerlo te puede explotar el palito en el aceite hirviendo y te podés quemar.

7) Dejalo reposar en el freezer por lo menos media hora, de ahí para arriba está perfecto

## Bastoncitos

1) Saca la masa toda congelada del freezer y cortala en bastoncitos, que no te queden muy grandes porque después no te entra en ningún lado. No se como decirte el tamaño, son como papas fritas con esteroides.

2) Poné una cantidad generosa de aceite a hervir, pensá que tenés que poder sumergir bien los bastoncitos, y mientras más bastoncitos te entren a la vez mejor.

3) Medio que ya es obvio lo que queda, anda hirviendo los bastoncitos hasta que queden dorados como papas fritas, dejá que se enfríen un toque y a comer bobby (no te olvides de romperte esos dips).